
var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Cualidades del negociador",
    autor: "Edilson Laverde Molina",
    date:  "10/03/2021",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios=[
    {url:"sonidos/click.mp3",      name:"clic"  },
    {url:"sonidos/fail.mp3",       name:"fail"  },
    {url:"sonidos/good.mp3",       name:"good"  },
    {url:"sonidos/intro.mp3",      name:"intro" },
    {url:"sonidos/intro1.mp3",     name:"intro1" },
    {url:"sonidos/intro2.mp3",     name:"intro2" },
    {url:"sonidos/intro3.mp3",     name:"intro3" },
];
var module="g0";
var good2=0;
var data_solution=[];
var soluctions = [
    {
        "termino":     "Ser empático",
        "significado": "Ponerse en el lugar del otro, tener la facilidad de entender las necesidad de la contraparte.",
        "box":"box1"
    },
    {
        "termino":     "Escuchar de forma activa",
        "significado": "Técnica y estrategia de la comunicación que permite a un interlocutor sentirse escuchado, valorado, entendido. Quien usa esta técnica capta toda la información haciendo uso de todos sus sentidos.",
        "box":"box4"
    },
    {
        "termino":     "Tener confianza en sí mismo",
        "significado": "Sentirse seguro de sí mismo, de los talentos, habilidades y saberes que se posee. Brindar la serenidad y seguridad de ser una persona capaz.",
        "box":"box7"
    },
    {
        "termino":     "No tomarse una situación como personal",
        "significado": "No es posible apropiarse del conflicto como si fuera propio, y si así fuera, no es bueno ligar el conflicto a las personas, pues la emoción siempre hará que se pierda el objetivo (la negociación).",
        "box":"box2"
    },
    {
        "termino":     "Tener intención de ceder sin dejar de ganar",
        "significado": "Esta es una técnica o estrategia para lograr acuerdos dentro de un proceso de negociación. El ceder permite en ocasiones lograr la victoria.",
        "box":"box5"
    },
    {
        "termino":     "Respetar a la otra parte",
        "significado": "La dignidad personal como derecho fundamental no podrá ser olvidada en el momento de entrar a negociar, lo cual obliga a comprender y valorar la dignidad del otro.",
        "box":"box8"
    },
    {
        "termino":     "Controlar las emociones",
        "significado": "Es necesario tener serenidad, paciencia y saber en qué momento expresar los sentimientos para que no se trastornen los arreglos logrados.",
        "box":"box3"
    },
    {
        "termino":     "Capacidad comunicativa",
        "significado": "Habilidades como ser lacónico, congruente y persuasivo son preponderantes en un buen negociador.",
        "box":"box6"
    },
    {
        "termino":     "Flexibilidad",
        "significado": "Tiene que ver con la facilidad de cambiar la estrategia de negociación en cualquier momento con el propósito de lograr acuerdos.",
        "box":"box9"
    }
];
var estrategias = [
    {texto:"Siendo  neutral, no tomando partido, pensando en las partes.",                         "box":["box-2"]       },
    {texto:"Tomando decisiones propias.",                                                          "box":["box-7"]       },
    {texto:"Desarrollando la costumbre de escuchar con atención a los demás.",                     "box":["box-4"]       },
    {texto:"Tratando de no usar su celular mientras interactúo con otros.",                        "box":["box-4"]       },
    {texto:"Concentrándome en lo que los demás dicen.",                                            "box":["box-4"]       },
    {texto:"Obligándome a guardar silencio mientras los otros hablan.",                            "box":["box-4","box-3"]},
    {texto:"Esperando mi momento para hablar." ,                                                   "box":["box-4"]       },
    {texto:"No haciendo parte de discusiones inútiles.",                                           "box":["box-3"]       },
    {texto:"Tratando de ser paciente. Serenándome. No iniciando discusiones.",                     "box":["box-3"]       },
    {texto:"Tratando de no ser radical en las negociaciones.",                                     "box":["box-5","box-9"]       },
    {texto:"Tratando de entender a los demás y asegurándome de que esto sea una costumbre en mi.", "box":["box-1"]       },
    {texto:"Siendo concreto, claro, puntual cuando hablo." ,                                       "box":["box-6"]       },
    {texto:"No actuando de forma visceral.",                                                       "box":["box-3"]       },
    {texto:"Valorando al otro, comprendiendo su calidad de ser humano.",                           "box":["box-8"]       },
    {texto:"Analizando muy bien las situaciones antes de actuar y hablar.",                        "box":["box-9"]       },
]
var index=0;
function main(sym) {
var items=[];

var udec = ivo.structure({
        created:function(){
           var t=this;
           soluctions = soluctions.sort(function() {return Math.random() - 0.5});
            //precarga audios//
           ivo.load_audio(audios,onComplete=function(){
               t.set_labels();
               t.animation();
               t.events();
               ivo(ST+"preload").hide();
               stage1.play();
               t.launch_terminos();
           });
        },
        methods: {
            set_labels:function(){
                for(s of soluctions){
                    console.log(s.termino);
                    $(ST+s.box).attr("data-termino",s.termino);
                }
            },
            set_options:function(){
                $(ST+"html-select").html("<select style='width:80%' id='select' name='select'></select><button id='add' style='margin-left:2%; width:14%'  type='button'>Agregar</button><ul id='box'></ul>");
                $("#add").on("click",function(){
                    if($("#select").val()!=null){
                        $("#box").append("<li>"+$("#select").val()+"</li>");
                    }
                    $('#select option[value="'+$("#select").val()+'"').remove();
                });
                for(s of estrategias){
                    for(s2 of s.box){
                        for(s1 of data_solution){
                            if(s1==s2){
                                $("#select").append(`<option value="${s.texto}" >${s.texto}</option>`);
                            }
                        }
                    }
                }
            },
            launch_terminos:function(){
                try{
                    $(ST+"t1").html(`${soluctions[index].significado}`);
                    $(".contenedor1").attr("data-termino",soluctions[index].termino);
                    $( ST+"Recurso_30" ).attr("data-termino",soluctions[index].termino);
                    TweenMax.from(ST+"t1", .8,       {x:-100,opacity:0})
                }catch (error) {
                    $(ST+"t1").hide();
                    $(ST+"home2").addClass("animated infinite flash");
                    var nota = ((25/9)*good2)+25;
                    Scorm_mx = new MX_SCORM(false);
                    console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id+" Nota: "+nota);
                    Scorm_mx.set_score(nota);
                    if(good2==0 || good2==1 || good2==2){
                        $(ST+"Text3").html(`Ánimo, siga intentándolo, Es importante revisar los recursos educativos del curso para fortalecer sus conocimientos. Nota: `+nota);
                    }
                    if(good2==3 || good2==4){
                        $(ST+"Text3").html(`Ha sido un buen esfuerzo. Sin embargo, es necesario revisar los recursos educativos del curso para construir conocimientos nuevos. Nota: `+nota);
                    }
                    if(good2==5 || good2==6 || good2==7){
                        $(ST+"Text3").html(`Muy bien, ha sido un buen trabajo.`);
                    }
                    if(good2==8 || good2==9){
                        $(ST+"Text3").html(`Excelente, felicidades, Conoce muy bien las cualidades del buen negociador, impleméntelas en la cotidianidad personal y laboral. Nota: `+nota);
                    }
                }
            },
            events:function(){
                var t=this;
                //$(ST+"grupo_2,"+ST+"grupo2").hide();
                //$(ST+"grupo_3,"+ST+"grupo3").hide();
                $(ST+"btn_iniciar").on("click",function(){
                    stage1.reverse().timeScale(30);
                    stage2.play().timeScale(1);
                    ivo.play("clic");
                });
                $(ST+"btn_instruccion").on("click",function(){
                    ivo.play("intro");
                });
                $(ST+"btn_atras").on("click",function(){
                    stage2.reverse().timeScale(30);
                    stage1.play().timeScale(1);
                    ivo.play("clic");
                });
                $(ST+"btn_creditos").on("click",function(){
                    creditos.play().timeScale(1);
                    ivo.play("clic");
                });

                $(ST+"close_1").on("click",function(){
                    creditos.reverse().timeScale(10);
                    ivo.play("clic");
                });

                $(ST+"close_3").on("click",function(){
                    select.reverse().timeScale(10);
                    ivo.play("clic");
                });
                $(ST+"close_4").on("click",function(){
                    objetivo.reverse().timeScale(10);
                    ivo.play("clic");
                });
                $(ST+"btn_objetivo").on("click",function(){
                    objetivo.play().timeScale(1);
                    ivo.play("clic");
                });

                $(ST+"home2").on("click",function(){
                    window[module].reverse().timeScale(10);
                    globo1.reverse();
                    globo2.reverse();
                    globo3.reverse();
                    stage3.reverse().timeScale(30);
                    if(module=="g3"){
                        setTimeout(function(){
                            final.play();
                        },3000);
                    }
                    ivo.play("clic");
                });
                $(ST+"b_1").addClass("animated infinite tada");
                $(ST+"b_1").on("click",function(){
                    module="g1";
                    $(this).removeClass("animated infinite tada");
                    $(ST+"b_2").addClass("animated infinite tada");
                    stage3.play().timeScale(1);
                    ivo.play("clic");
                });
                $(ST+"b_2").on("click",function(){
                    if(module=="g1" || module=="g2" || module=="g3"){
                        module="g2";
                        $(ST+"b_2").removeClass("animated infinite tada");
                        $(ST+"b_3").addClass("animated infinite tada");
                        stage3.play().timeScale(1);
                        ivo.play("clic");
                    }
                });
                $(ST+"b_3").on("click",function(){
                    if(module=="g2" || module=="g3"){
                        module="g3";
                        $(this).removeClass("animated infinite tada");
                        stage3.play().timeScale(1);
                        ivo.play("clic");
                    }
                });

                $(ST+"Group2").on("click",function(){
                    select.play().timeScale(1);
                    ivo.play("clic");
                });

                $( ".box1" ).draggable({revert: true});
                $( ST+"Recurso_30" ).droppable({
                    drop: function( event, ui ) {
                        $(ui.draggable).hide();
                        if($(ui.draggable).attr("data-termino")==$(this).attr("data-termino")){
                            ivo.play("good");
                            good2+=1;
                        }else{
                            ivo.play("fail");
                        }
                        index+=1;
                        t.launch_terminos();
                    }
                });
                $( ".box2" ).draggable({revert: true});
                $( ".contenedor2" ).droppable({
                    drop: function( event, ui ) {
                        $(ui.draggable).hide();
                        ivo.play("good");
                    }
                });
                $( ".box3" ).draggable({revert: true});
                $( ".contenedor3" ).droppable({
                    drop: function( event, ui ) {
                        $(ui.draggable).hide();
                        data_solution.push($(ui.draggable).attr("id").split("Stage_")[1]);
                        t.set_options();
                    }
                });

                $(ST+"close_2").on("click",function(){
                    switch (module) {
                        case "g1":
                            globo1.reverse();
                            break;
                        case "g2":
                            globo3.reverse();
                            break;
                        case "g3":
                            globo2.reverse();
                            break;
                    }
                    ivo.play("clic");
                });
            },
            globos:function(name,box){
                window[name]= new TimelineMax();
                window[name].append(TweenMax.from(box, .4,                    {scale:0,opacity:0}), 0);
                window[name].append(TweenMax.fromTo(ST+"close_2", .4,         {display:"none",opacity:0,rotation:900},{display:"block",opacity:1,rotation:0}), 0);
                window[name].stop();
            },
            grupos:function(name,box,box2){
                window[name]= new TimelineMax({onComplete:function(){
                    switch (module) {
                        case "g1":
                            globo1.play();
                            break;
                        case "g2":
                            globo3.play();
                            break;
                        case "g3":
                            globo2.play();
                            break;
                    }
                }});
                window[name].append(TweenMax.fromTo(ST+"avatar", .4,          {x:-1000},{x:0}), 0);
                window[name].append(TweenMax.staggerFrom($(box), .3,          {x:3500,opacity:0,ease:Elastic.easeOut.config(0.3, 0.4)},-.1), 0);
                window[name].append(TweenMax.staggerFrom($(box + " div"), .3, {y:3500,opacity:0,ease:Elastic.easeOut.config(0.3, 0.4)},-.1), 0);
                window[name].append(TweenMax.from(box2, .4,                   {x:3200,opacity:0}), 0);
                window[name].append(TweenMax.fromTo(ST+"home2", .4,           {x:-1000},{x:0}), 0);
                window[name].stop();
            },
            animation:function(){
                stage1 = new TimelineMax({
                    onComplete:function(){
                        $(ST+"btn_iniciar").addClass("animated infinite flash");
                    }
                });
                stage1.append(TweenMax.from(ST+"stage1", .8,          {x:1300,opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"Recurso_4", .8,       {x:-600,opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"Recurso_12", .8,      {scaleY:0,opacity:0}), 0);
                //stage1.append(TweenMax.staggerFrom(".obj1",.4,        {x:1500,opacity:0,y:-800,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage1.append(TweenMax.from(ST+"Recurso_3", .8,       {opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"btn_iniciar", .8,     {x:-1300,opacity:0}), 0);
                stage1.stop();
                stage2 = new TimelineMax({onComplete:function(){
                    ivo.play("intro");
                }});
                stage2.append(TweenMax.from(ST+"stage2", .8,          {x:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"Recurso_122", .8,     {x:-1300,opacity:0}), 0);
                stage2.append(TweenMax.staggerFrom(".manos",.4,       {y:1500,opacity:0,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage2.append(TweenMax.staggerFrom(".iconos",.4,      {scale:0,rotation:900,opacity:0,x:200,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage2.append(TweenMax.staggerFrom(".n",.4,           {scale:0,rotation:900,opacity:0,y:-800,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage2.append(TweenMax.staggerFrom(".btn",.4,         {x:300,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage2.append(TweenMax.from(ST+"Recurso_15Copy2", .8, {x:-1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"btn_sig_1", .8,       {x:1300,opacity:0}), 0);
                stage2.stop();
                this.globos("globo1",ST+"msg1");
                this.globos("globo2",ST+"msg2");
                this.globos("globo3",ST+"msg3");
                this.grupos("g1",ST+"grupo1",ST+"grupo_1");
                this.grupos("g2",ST+"grupo2",ST+"grupo_2");
                this.grupos("g3",ST+"grupo3",ST+"grupo_3");
                stage3 = new TimelineMax({onComplete:function(){
                    window[module].play().timeScale(1);
                    if(module=="g1"){
                        ivo.play("intro1");
                    }
                    if(module=="g2"){
                        ivo.play("intro3");
                    }
                    if(module=="g3"){
                        ivo.play("intro2");
                    }
                }});
                stage3.append(TweenMax.from(ST+"stage3", .8,        {x:-1300,opacity:0}), 0);
                stage3.stop();

                creditos = new TimelineMax();
                creditos.append(TweenMax.from(ST+"creditos", .8,    {x:1300,opacity:0}), 0);
                creditos.append(TweenMax.from(ST+"close_1", .8,     {rotation:1300,scale:0,opacity:0}), 0);
                creditos.stop();

                objetivo = new TimelineMax();
                objetivo.append(TweenMax.from(ST+"objetivo", .8,    {x:1300,opacity:0}), 0);
                objetivo.append(TweenMax.from(ST+"close_4", .8,     {rotation:1300,scale:0,opacity:0}), 0);
                objetivo.stop();

                final = new TimelineMax();
                final.append(TweenMax.from(ST+"final", .8,          {x:1300,opacity:0}), 0);
                final.stop();

                select = new TimelineMax();
                select.append(TweenMax.from(ST+"select", .8,        {x:1300,opacity:0}), 0);
                select.append(TweenMax.from(ST+"close_3", .8,     {rotation:1300,scale:0,opacity:0}), 0);
                select.stop();

            }
        }
 });
}

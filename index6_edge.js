/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['0', '0', '1024', '671', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-intro',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-intro.jpg",'0px','0px']
                            },
                            {
                                id: 'Recurso_3',
                                type: 'image',
                                rect: ['189px', '354px', '412px', '317px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_3.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_12',
                                type: 'image',
                                rect: ['361px', '92px', '418px', '506px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_5',
                                type: 'image',
                                rect: ['59px', '313px', '886px', '300px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_5.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_4',
                                type: 'image',
                                rect: ['0px', '58px', '395px', '163px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_4.png",'0px','0px']
                            },
                            {
                                id: 'btn_iniciar',
                                type: 'image',
                                rect: ['158px', '212px', '113px', '113px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_2.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['-52', '0', '1084', '683', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondos_pantallas',
                                type: 'image',
                                rect: ['52px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondos_pantallas.png",'0px','0px']
                            },
                            {
                                id: 'btn_atras',
                                type: 'image',
                                rect: ['62px', '35px', '76px', '95px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_6.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_43',
                                type: 'image',
                                rect: ['642px', '81px', '439px', '68px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_43.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_14',
                                type: 'image',
                                rect: ['0px', '274px', '289px', '402px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_14.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_122',
                                type: 'image',
                                rect: ['293px', '240px', '541px', '219px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_12.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_11',
                                type: 'image',
                                rect: ['733px', '344px', '84px', '339px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_11.png",'0px','0px'],
                                userClass: "manos"
                            },
                            {
                                id: 'Recurso_10',
                                type: 'image',
                                rect: ['528px', '407px', '84px', '267px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_10.png",'0px','0px'],
                                userClass: "manos"
                            },
                            {
                                id: 'Recurso_9',
                                type: 'image',
                                rect: ['273px', '475px', '85px', '199px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_9.png",'0px','0px'],
                                userClass: "manos"
                            },
                            {
                                id: 'Recurso_7',
                                type: 'image',
                                rect: ['854px', '11px', '222px', '92px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px']
                            },
                            {
                                id: 'b_1',
                                type: 'group',
                                rect: ['253', '335', '87', '140', 'auto', 'auto'],
                                cursor: 'pointer',
                                c: [
                                {
                                    id: 'Recurso_13',
                                    type: 'image',
                                    rect: ['18px', '87px', '53px', '53px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_13.png",'0px','0px'],
                                    userClass: "iconos"
                                },
                                {
                                    id: '_1',
                                    type: 'image',
                                    rect: ['0px', '0px', '87px', '87px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"1.svg",'0px','0px'],
                                    userClass: "n"
                                }]
                            },
                            {
                                id: 'b_2',
                                type: 'group',
                                rect: ['507', '232', '87', '139', 'auto', 'auto'],
                                cursor: 'pointer',
                                c: [
                                {
                                    id: 'Recurso_13Copy',
                                    type: 'image',
                                    rect: ['17px', '86px', '53px', '53px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_13.png",'0px','0px'],
                                    userClass: "iconos"
                                },
                                {
                                    id: '_2',
                                    type: 'image',
                                    rect: ['0px', '0px', '87px', '88px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"2.svg",'0px','0px'],
                                    userClass: "n"
                                }]
                            },
                            {
                                id: 'b_3',
                                type: 'group',
                                rect: ['703', '158', '87', '140', 'auto', 'auto'],
                                cursor: 'pointer',
                                c: [
                                {
                                    id: 'Recurso_13Copy2',
                                    type: 'image',
                                    rect: ['19px', '87px', '53px', '53px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_13.png",'0px','0px'],
                                    userClass: "iconos"
                                },
                                {
                                    id: '_3',
                                    type: 'image',
                                    rect: ['0px', '0px', '87px', '87px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"3.svg",'0px','0px'],
                                    userClass: "n"
                                }]
                            }]
                        },
                        {
                            id: 'stage3',
                            type: 'group',
                            rect: ['0px', '0px', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle',
                                type: 'rect',
                                rect: ['0px', '-7px', '1035px', '662px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'fondos_pantallasCopy',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondos_pantallas.png",'0px','0px']
                            },
                            {
                                id: 'avatar',
                                symbolName: 'avatar',
                                type: 'rect',
                                rect: ['20', '335', '169', '530', 'auto', 'auto']
                            },
                            {
                                id: 'home2',
                                type: 'image',
                                rect: ['8px', '7px', '77px', '96px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_31.png",'0px','0px']
                            },
                            {
                                id: 'globos',
                                type: 'group',
                                rect: ['0', '123', '281', '236', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'msg1',
                                    type: 'image',
                                    rect: ['0px', '2px', '274px', '219px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_16.png",'0px','0px']
                                },
                                {
                                    id: 'msg2',
                                    type: 'image',
                                    rect: ['0px', '-6px', '274px', '249px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_41.png",'0px','0px']
                                },
                                {
                                    id: 'msg3',
                                    type: 'image',
                                    rect: ['0px', '-6px', '274px', '219px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_40.png",'0px','0px']
                                },
                                {
                                    id: 'close_2',
                                    type: 'image',
                                    rect: ['239px', '-7px', '50px', '51px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'grupo_3',
                                type: 'group',
                                rect: ['284', '359', '592', '254', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Group2',
                                    type: 'group',
                                    rect: ['-22', '141', '614', '113', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    c: [
                                    {
                                        id: 'Recurso_32Copy2',
                                        type: 'image',
                                        rect: ['22px', '0px', '592px', '113px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_32.png",'0px','0px']
                                    },
                                    {
                                        id: 'Recurso_33',
                                        type: 'image',
                                        rect: ['522px', '20px', '73px', '73px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_33.png",'0px','0px']
                                    },
                                    {
                                        id: 'Text2Copy3',
                                        type: 'text',
                                        rect: ['0px', '41px', '562px', '38px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​Haga clic en la flecha para desplegar las estretegias</p>",
                                        align: "center",
                                        font: ['Arial, Helvetica, sans-serif', [19, "px"], "rgba(43,71,139,1.00)", "700", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    }]
                                },
                                {
                                    id: 'Text2Copy2',
                                    type: 'text',
                                    rect: ['14px', '109px', '562px', '38px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​¿Cómo planea usted desarrollar dichas cualidades?</p>",
                                    align: "center",
                                    font: ['Arial, Helvetica, sans-serif', [19, "px"], "rgba(43,71,139,1.00)", "700", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'Group',
                                    type: 'group',
                                    rect: ['0', '0', '592', '113', 'auto', 'auto'],
                                    userClass: "contenedor3",
                                    c: [
                                    {
                                        id: 'Recurso_32Copy',
                                        type: 'image',
                                        rect: ['0px', '0px', '592px', '113px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_32.png",'0px','0px']
                                    },
                                    {
                                        id: 'Text2Copy',
                                        type: 'text',
                                        rect: ['199px', '40px', '187px', '38px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​Arrastre aquí</p>",
                                        align: "center",
                                        font: ['Arial, Helvetica, sans-serif', [19, "px"], "rgba(43,71,139,1.00)", "700", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    }]
                                }]
                            },
                            {
                                id: 'grupo_2',
                                type: 'group',
                                rect: ['287', '447', '592', '113', 'auto', 'auto'],
                                userClass: "contenedor2",
                                c: [
                                {
                                    id: 'Recurso_32',
                                    type: 'image',
                                    rect: ['0px', '0px', '592px', '113px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_32.png",'0px','0px']
                                },
                                {
                                    id: 'Text2',
                                    type: 'text',
                                    rect: ['202px', '34px', '187px', '38px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​Arrastre aquí</p>",
                                    align: "center",
                                    font: ['Arial, Helvetica, sans-serif', [20, "px"], "rgba(43,71,139,1.00)", "700", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                }]
                            },
                            {
                                id: 'grupo_1',
                                type: 'group',
                                rect: ['214', '447', '665', '113', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_30',
                                    type: 'image',
                                    rect: ['0px', '21px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_30.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_29',
                                    type: 'image',
                                    rect: ['235px', '-29px', '405px', '166px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_29.png",'0px','0px'],
                                    userClass: "contenedor1"
                                },
                                {
                                    id: 't1',
                                    type: 'text',
                                    rect: ['269px', '30px', '329px', '58px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "center",
                                    font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                }]
                            },
                            {
                                id: 'grupo1',
                                type: 'group',
                                rect: ['319', '177', '527', '172', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'box9',
                                    type: 'image',
                                    rect: ['359px', '120px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_28.png",'0px','0px'],
                                    userClass: "box1"
                                },
                                {
                                    id: 'box8',
                                    type: 'image',
                                    rect: ['180px', '120px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_27.png",'0px','0px'],
                                    userClass: "box1"
                                },
                                {
                                    id: 'box7',
                                    type: 'image',
                                    rect: ['0px', '120px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_26.png",'0px','0px'],
                                    userClass: "box1"
                                },
                                {
                                    id: 'box6',
                                    type: 'image',
                                    rect: ['359px', '59px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_25.png",'0px','0px'],
                                    userClass: "box1"
                                },
                                {
                                    id: 'box5',
                                    type: 'image',
                                    rect: ['180px', '59px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_24.png",'0px','0px'],
                                    userClass: "box1"
                                },
                                {
                                    id: 'box4',
                                    type: 'image',
                                    rect: ['0px', '59px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_23.png",'0px','0px'],
                                    userClass: "box1"
                                },
                                {
                                    id: 'box3',
                                    type: 'image',
                                    rect: ['359px', '0px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_22.png",'0px','0px'],
                                    userClass: "box1"
                                },
                                {
                                    id: 'box2',
                                    type: 'image',
                                    rect: ['180px', '0px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_21.png",'0px','0px'],
                                    userClass: "box1"
                                },
                                {
                                    id: 'box1',
                                    type: 'image',
                                    rect: ['0px', '0px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px'],
                                    userClass: "box1"
                                }]
                            },
                            {
                                id: 'grupo2',
                                type: 'group',
                                rect: ['319', '177', '527', '172', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'box_9',
                                    type: 'image',
                                    rect: ['359px', '120px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_28.png",'0px','0px'],
                                    userClass: "box2"
                                },
                                {
                                    id: 'box_8',
                                    type: 'image',
                                    rect: ['180px', '120px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_27.png",'0px','0px'],
                                    userClass: "box2"
                                },
                                {
                                    id: 'box_7',
                                    type: 'image',
                                    rect: ['0px', '120px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_26.png",'0px','0px'],
                                    userClass: "box2"
                                },
                                {
                                    id: 'box_6',
                                    type: 'image',
                                    rect: ['359px', '59px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_25.png",'0px','0px'],
                                    userClass: "box2"
                                },
                                {
                                    id: 'box_5',
                                    type: 'image',
                                    rect: ['180px', '59px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_24.png",'0px','0px'],
                                    userClass: "box2"
                                },
                                {
                                    id: 'box_4',
                                    type: 'image',
                                    rect: ['0px', '59px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_23.png",'0px','0px'],
                                    userClass: "box2"
                                },
                                {
                                    id: 'box_3',
                                    type: 'image',
                                    rect: ['359px', '0px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_22.png",'0px','0px'],
                                    userClass: "box2"
                                },
                                {
                                    id: 'box_2',
                                    type: 'image',
                                    rect: ['180px', '0px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_21.png",'0px','0px'],
                                    userClass: "box2"
                                },
                                {
                                    id: 'box_1',
                                    type: 'image',
                                    rect: ['0px', '0px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px'],
                                    userClass: "box2"
                                }]
                            },
                            {
                                id: 'grupo3',
                                type: 'group',
                                rect: ['319', '177', '527', '172', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'box-9',
                                    type: 'image',
                                    rect: ['359px', '120px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_28.png",'0px','0px'],
                                    userClass: "box3"
                                },
                                {
                                    id: 'box-8',
                                    type: 'image',
                                    rect: ['180px', '120px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_27.png",'0px','0px'],
                                    userClass: "box3"
                                },
                                {
                                    id: 'box-7',
                                    type: 'image',
                                    rect: ['0px', '120px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_26.png",'0px','0px'],
                                    userClass: "box3"
                                },
                                {
                                    id: 'box-6',
                                    type: 'image',
                                    rect: ['359px', '59px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_25.png",'0px','0px'],
                                    userClass: "box3"
                                },
                                {
                                    id: 'box-5',
                                    type: 'image',
                                    rect: ['180px', '59px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_24.png",'0px','0px'],
                                    userClass: "box3"
                                },
                                {
                                    id: 'box-4',
                                    type: 'image',
                                    rect: ['0px', '59px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_23.png",'0px','0px'],
                                    userClass: "box3"
                                },
                                {
                                    id: 'box-3',
                                    type: 'image',
                                    rect: ['359px', '0px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_22.png",'0px','0px'],
                                    userClass: "box3"
                                },
                                {
                                    id: 'box-2',
                                    type: 'image',
                                    rect: ['180px', '0px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_21.png",'0px','0px'],
                                    userClass: "box3"
                                },
                                {
                                    id: 'box-1',
                                    type: 'image',
                                    rect: ['0px', '0px', '168px', '52px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px'],
                                    userClass: "box3"
                                }]
                            },
                            {
                                id: 'Recurso_43Copy',
                                type: 'image',
                                rect: ['590px', '81px', '439px', '68px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_43.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_7Copy',
                                type: 'image',
                                rect: ['802px', '11px', '222px', '92px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'btn_creditos',
                            type: 'image',
                            rect: ['923px', '232px', '58px', '73px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"Recurso_37.png",'0px','0px'],
                            userClass: "btn"
                        },
                        {
                            id: 'btn_instruccion',
                            type: 'image',
                            rect: ['881px', '309px', '148px', '144px', 'auto', 'auto'],
                            clip: 'rect(0px 122px 116px 31px)',
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"Recurso_38.png",'0px','0px'],
                            userClass: "btn"
                        },
                        {
                            id: 'btn_objetivo',
                            type: 'image',
                            rect: ['884px', '422px', '148px', '144px', 'auto', 'auto'],
                            clip: 'rect(0px 124px 118px 24px)',
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"Recurso_39.png",'0px','0px'],
                            userClass: "btn"
                        },
                        {
                            id: 'creditos',
                            type: 'group',
                            rect: ['0px', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-opacidad',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-opacidad.png",'0px','0px']
                            },
                            {
                                id: 'tablero-texto',
                                type: 'image',
                                rect: ['163px', '46px', '698px', '593px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"tablero-texto.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_432',
                                type: 'image',
                                rect: ['792px', '202px', '237px', '439px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_432.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_42',
                                type: 'image',
                                rect: ['0px', '271px', '232px', '368px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_42.png",'0px','0px']
                            },
                            {
                                id: 'Text2Copy4',
                                type: 'text',
                                rect: ['312px', '92px', '411px', '414px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-weight: 900;\">Nombre del material</span></p><p style=\"margin:0px\">​​Características del negociador</p><p style=\"margin: 0px;\"><span style=\"font-weight: 900;\"></span></p><p style=\"margin:0px\">​</p><p style=\"margin: 0px;\"><span style=\"font-weight: 700;\">Diseño instruccional</span></p><p style=\"margin: 0px;\">​ &nbsp; Lida Consuelo Rincón Méndez</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\"><span style=\"font-weight: 900;\">Experto en contenido</span></p><p style=\"margin: 0px;\">​Sonia Smith Niño</p><p style=\"margin: 0px;\">​</p><p style=\"margin:0px\">​</p><p style=\"margin: 0px; font-family: Arial, Helvetica, sans-serif; font-weight: 900; font-style: normal; text-decoration: none; font-size: 15px; color: rgb(49, 63, 92); background-color: rgba(0, 0, 0, 0); letter-spacing: 0px; text-transform: none; word-spacing: 0px; text-align: center; text-indent: 0px; line-height: normal;\">Diseño</p><p style=\"margin: 0px; font-family: Arial, Helvetica, sans-serif; font-weight: 400; font-style: normal; text-decoration: none; font-size: 15px; color: rgb(49, 63, 92); background-color: rgba(0, 0, 0, 0); letter-spacing: 0px; text-transform: none; word-spacing: 0px; text-align: center; text-indent: 0px; line-height: normal;\">Nazly María Victoria Díaz Vera</p><p style=\"margin: 0px; font-family: Arial, Helvetica, sans-serif; font-weight: 400; font-style: normal; text-decoration: none; font-size: 15px; color: rgb(49, 63, 92); background-color: rgba(0, 0, 0, 0); letter-spacing: 0px; text-transform: none; word-spacing: 0px; text-align: center; text-indent: 0px; line-height: normal;\">​</p><p style=\"margin: 0px;\"><span style=\"font-weight: 900;\">Programación</span></p><p style=\"margin: 0px;\">Edilson Laverde</p><p style=\"margin: 0px;\"><span style=\"font-weight: 900;\"></span></p><p style=\"margin:0px\">​</p><p style=\"margin: 0px;\"><span style=\"font-weight: 900;\">Sonidos</span></p><p style=\"margin: 0px;\"></p><p style=\"margin: 0px; font-family: Arial, Helvetica, sans-serif; font-weight: 400; font-style: normal; text-decoration: none; font-size: 15px; color: rgb(49, 63, 92); background-color: rgba(0, 0, 0, 0); letter-spacing: 0px; text-transform: none; word-spacing: 0px; text-align: center; text-indent: 0px; line-height: normal;\">&nbsp;&nbsp; Lida Consuelo Rincón Méndez</p><p style=\"margin: 0px; font-family: Arial, Helvetica, sans-serif; font-weight: 400; font-style: normal; text-decoration: none; font-size: 15px; color: rgb(49, 63, 92); background-color: rgba(0, 0, 0, 0); letter-spacing: 0px; text-transform: none; word-spacing: 0px; text-align: center; text-indent: 0px; line-height: normal;\">​</p><p style=\"margin: 0px;\"><span style=\"font-weight: 900;\">Oficina de Educación Virtual y a Distancia</span></p><p style=\"margin: 0px;\"><span style=\"font-weight: 900;\">Universidad de Cundinamarca</span></p><p style=\"margin: 0px;\"><span style=\"font-weight: 900;\">2021</span></p><p style=\"margin: 0px;\">&nbsp;</p><p style=\"margin:0px\">​</p><p style=\"margin: 0px;\"><span style=\"font-weight: 400; font-size: 15px;\"></span></p>",
                                align: "center",
                                font: ['Arial, Helvetica, sans-serif', [15, "px"], "rgba(49,63,92,1.00)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'close_1',
                                type: 'image',
                                rect: ['810px', '48px', '50px', '51px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'objetivo',
                            type: 'group',
                            rect: ['0px', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-opacidadCopy3',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-opacidad.png",'0px','0px']
                            },
                            {
                                id: 'tablero-textoCopy',
                                type: 'image',
                                rect: ['163px', '46px', '698px', '593px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"tablero-texto.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_432Copy3',
                                type: 'image',
                                rect: ['792px', '202px', '237px', '439px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_432.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_42Copy3',
                                type: 'image',
                                rect: ['0px', '271px', '232px', '368px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_42.png",'0px','0px']
                            },
                            {
                                id: 'Text2Copy5',
                                type: 'text',
                                rect: ['298px', '161px', '440px', '57px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\"><span style=\"font-size: 25px; font-weight: 700;\">OBJETIVO</span></p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\">Descubrir las cualidades de negociador que se tienen.</p><p style=\"margin: 0px;\"><span style=\"font-weight: 700;\">​</span></p><p style=\"margin: 0px;\"><span style=\"font-weight: 700;\">​</span></p><p style=\"margin:0px\"><span style=\"font-weight: 700;\">​</span></p><p style=\"margin: 0px;\"><span style=\"font-weight: 400; font-size: 15px;\"></span></p>",
                                align: "justify",
                                font: ['Arial, Helvetica, sans-serif', [17, "px"], "rgba(49,63,92,1.00)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'close_4',
                                type: 'image',
                                rect: ['810px', '48px', '50px', '51px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'final',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-opacidadCopy',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-opacidad.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_432Copy',
                                type: 'image',
                                rect: ['792px', '202px', '237px', '439px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_432.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_42Copy',
                                type: 'image',
                                rect: ['0px', '271px', '232px', '368px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_42.png",'0px','0px']
                            },
                            {
                                id: 'tablero-retroalimentacion',
                                type: 'image',
                                rect: ['226px', '95px', '594px', '409px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"tablero-retroalimentacion.png",'0px','0px']
                            },
                            {
                                id: 'Text',
                                type: 'text',
                                rect: ['264px', '208px', '520px', '198px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px; text-align: justify;\">​Realimentación final: El negociador,&nbsp;o el tercero neutral,&nbsp;siempre será aquel mediador&nbsp;imparcial&nbsp;que logra acuerdos,&nbsp;pues tiene siempre claras las alternativas,&nbsp;hace uso de sus cualidades y habilidades, motiva la comunicación&nbsp;con el único propósito de que&nbsp;la negociación a la que se llegue&nbsp;sea pacífica y equitativa&nbsp;entre las partes, sin generar sentimientos de derrota. &nbsp;</p><p style=\"margin: 0px; text-align: justify;\">​</p><p style=\"margin: 0px; text-align: justify;\">Cada uno de nosotros tenemos algunas de las cualidades del negociador, pero en nuestra área laboral son muchas las dificultades que se nos pueden presentar, por lo cual, estamos llamados a aprender aquellas cualidades que no poseemos y para ello es necesaria la práctica. Así que ánimo, implemente aquellas cualidades de negociador que posee, así como las que ha decidido aprender y recuerde: <span style=\"font-weight: 900;\">¡la práctica hace al maestro!</span></p><p style=\"margin: 0px; text-align: justify;\"><span style=\"font-weight: 900;\">​</span></p>",
                                align: "center",
                                font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'Text3',
                                type: 'text',
                                rect: ['258px', '412px', '529px', '46px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​</p>",
                                align: "center",
                                font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'Recurso_35',
                                type: 'image',
                                rect: ['548px', '128px', '84px', '74px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_35.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_34',
                                type: 'image',
                                rect: ['447px', '123px', '84px', '74px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_34.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'select',
                            type: 'group',
                            rect: ['0', '0px', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-opacidadCopy2',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-opacidad.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_432Copy2',
                                type: 'image',
                                rect: ['792px', '202px', '237px', '439px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_432.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_42Copy2',
                                type: 'image',
                                rect: ['0px', '271px', '232px', '368px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_42.png",'0px','0px']
                            },
                            {
                                id: 'tablero-retroalimentacionCopy',
                                type: 'image',
                                rect: ['226px', '95px', '594px', '409px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"tablero-retroalimentacion.png",'0px','0px']
                            },
                            {
                                id: 'html-select',
                                type: 'rect',
                                rect: ['262px', '125px', '516px', '339px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'close_3',
                                type: 'image',
                                rect: ['787px', '76px', '50px', '51px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '272px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 9000,
                    autoPlay: true,
                    data: [
                        [
                            "eid444",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg1}",
                            [67,97],
                            [67,97],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid647",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg1}",
                            [67,97],
                            [67,97],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid648",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg1}",
                            [67,97],
                            [67,97],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid649",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${msg1}",
                            [67,97],
                            [67,97],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid650",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg1}",
                            [67,97],
                            [67,97],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid651",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg1}",
                            [67,97],
                            [67,97],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid1",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_12}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid652",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_12}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid653",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_12}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid654",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${Recurso_12}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid655",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_12}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid656",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_12}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid446",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg3}",
                            [65,100],
                            [65,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid657",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg3}",
                            [65,100],
                            [65,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid658",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg3}",
                            [65,100],
                            [65,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid659",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${msg3}",
                            [65,100],
                            [65,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid660",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg3}",
                            [65,100],
                            [65,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid661",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg3}",
                            [65,100],
                            [65,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid445",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg2}",
                            [67,97],
                            [67,97],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid662",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg2}",
                            [67,97],
                            [67,97],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid663",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg2}",
                            [67,97],
                            [67,97],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid664",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${msg2}",
                            [67,97],
                            [67,97],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid665",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg2}",
                            [67,97],
                            [67,97],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid666",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${msg2}",
                            [67,97],
                            [67,97],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ]
                    ]
                }
            },
            "avatar": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '111px', '530px', 'auto', 'auto'],
                            id: 'Recurso_172',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_17.png', '0px', '0px']
                        },
                        {
                            rect: ['92px', '76px', '38px', '83px', 'auto', 'auto'],
                            id: 'Recurso_18',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_18.png', '0px', '0px']
                        },
                        {
                            transform: [[], ['42'], [0, 0, 0], [1, 1, 1]],
                            id: 'Recurso_19',
                            type: 'image',
                            rect: ['121px', '56px', '52px', '104px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_19.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '169px', '530px']
                        }
                    }
                },
                timeline: {
                    duration: 9000,
                    autoPlay: true,
                    data: [
                        [
                            "eid68",
                            "top",
                            0,
                            0,
                            "linear",
                            "${Recurso_19}",
                            '56px',
                            '56px'
                        ],
                        [
                            "eid71",
                            "rotateZ",
                            0,
                            2000,
                            "linear",
                            "${Recurso_19}",
                            '0deg',
                            '42deg'
                        ],
                        [
                            "eid73",
                            "rotateZ",
                            2000,
                            2000,
                            "linear",
                            "${Recurso_19}",
                            '42deg',
                            '0deg'
                        ],
                        [
                            "eid69",
                            "left",
                            0,
                            0,
                            "linear",
                            "${Recurso_19}",
                            '121px',
                            '121px'
                        ],
                        [
                            "eid67",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_19}",
                            [14,90],
                            [14,90],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid667",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_19}",
                            [14,90],
                            [14,90],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid668",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_19}",
                            [14,90],
                            [14,90],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid669",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${Recurso_19}",
                            [14,90],
                            [14,90],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid670",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_19}",
                            [14,90],
                            [14,90],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid671",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${Recurso_19}",
                            [14,90],
                            [14,90],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index6_edgeActions.js");
})("EDGE-13125226");
